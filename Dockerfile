FROM maven:3-eclipse-temurin-17 as build-stage
WORKDIR /app
COPY pom.xml .
RUN mvn dependency:go-offline
COPY src /app/src
RUN mvn package -DskipTests

FROM openjdk:17 as prod-stage
WORKDIR /app
COPY --from=build-stage /app/target/docker-simple-app-0.0.1-SNAPSHOT.jar .
EXPOSE 3000
ENTRYPOINT ["java","-jar","docker-simple-app-0.0.1-SNAPSHOT.jar"]
