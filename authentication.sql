/*
  IL EXISTE DEUX COMPTES
  COMPTE ADMINSTRATEUR INDENTIFIANT EST: admin:admin
  COMPTE COMMERCIAL INDENTIFIANT EST: commercial:commercial
*/
create table public.users(  
	id bigint NOT NULL,
    username character varying,
    password character varying
	);
	
INSERT INTO public.users(id, username, password)
	VALUES (1, 'commercial', '$2y$10$rwhv7pIpA9W6Ie/3UbNnx.maIm9A/GNkeDwRZmZqgFvgyX.Hq302a');
INSERT INTO public.users(id, username, password)
	VALUES (2, 'admin', '$2y$10$WNmIAz8Uc2h2S0Qv5tR3u.PdqUZSTw3eZ9Us36V2jocTQOhhaV5r6');
	
	
create table public.authorities(  
	id bigint NOT NULL,
    authority character varying(255),
    user_id bigint
	);

INSERT INTO public.authorities(id, authority, user_id) VALUES (1, 'ROLE_COMMERCIAL', 1);
INSERT INTO public.authorities(id, authority, user_id) VALUES (2, 'ROLE_ADMIN', 2);
INSERT INTO public.authorities(id, authority, user_id) VALUES (3, 'ROLE_COMMERCIAL', 2);
	