package com.example.dockersimpleapp.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class userController {

    @Autowired
    private UserService userService;

    // Create a new user
    @PostMapping
    public userModel createUser(@RequestBody userModel user) {
        return userService.createUser(user);
    }

    // Get all users
    @GetMapping
    public List<userModel> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/test")
    public String getTest() {
        return "test work fine!";
    }

    // Get user by ID
    @GetMapping("/{id}")
    public Optional<userModel> getUserById(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    // Update user by ID
    @PutMapping("/{id}")
    public userModel updateUser(@PathVariable Long id, @RequestBody userModel userDetails) {
        return userService.updateUser(id, userDetails);
    }

    // Delete all users
    @DeleteMapping
    public String deleteAllUsers() {
        userService.deleteAllUsers();
        return "All users have been deleted successfully.";
    }

    // Delete user by ID
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }
    
}
