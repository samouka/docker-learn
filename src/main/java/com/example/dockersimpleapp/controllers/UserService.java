package com.example.dockersimpleapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    // Create a new user
    public userModel createUser(userModel user) {
        return userRepository.save(user);
    }

    // Get all users
    public List<userModel> getAllUsers() {
        return userRepository.findAll();
    }

    // Get user by ID
    public Optional<userModel> getUserById(Long id) {
        return userRepository.findById(id);
    }

    // Update user
    public userModel updateUser(Long id, userModel userDetails) {
        Optional<userModel> user = userRepository.findById(id);
        if (user.isPresent()) {
            userModel existingUser = user.get();
            existingUser.setUsername(userDetails.getUsername());
            existingUser.setPassword(userDetails.getPassword());
            return userRepository.save(existingUser);
        }
        return null;
    }

    // Delete all users
    public void deleteAllUsers() {
        userRepository.deleteAll();
    }

    // Delete user
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    // Other business logic related to users
}