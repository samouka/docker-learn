package com.example.dockersimpleapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerSimpleAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerSimpleAppApplication.class, args);
	}

}
